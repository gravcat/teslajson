FROM python:3-alpine

ADD . /app/teslajson-src
    
WORKDIR /app/teslajson-src

RUN python setup.py install